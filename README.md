# ⚠️ This is a very crude prototype implementation that does not get developed anymore. A new tool with similar functionality exists at https://gitlab.com/feathecutie/svelte-to-uml

# Svelte UML Preprocessor

## Usage

`svelte.config.js`

```js
...
import umlPreprocess from 'svelte-uml-preprocessor';

/** @type {import('@sveltejs/kit').Config} */
const config = {
    preprocess: [..., umlPreprocess('uml.plantuml', 'plantuml'), ...],
    ...
}

export default config;
```

## Mermaid vs PlantUML

Although this preprocessor currently supports generating [Mermaid](https://mermaid.js.org) and [PlantUML](https://plantuml.com/) syntax,
the Mermaid implementation lacks many features that currently are not possible to implement due to appearent limitations in Mermaid.

I will try to extend the feature set (because Mermaid generally seems very cool), but for now PlantUML is strongly recommended.

## Preview

This is a rendered preview of the PlantUML output for the [SvelteKit Realword example app](https://github.com/sveltejs/realworld):
![Rendered preview](https://plantuml.feathecutie.de/plantuml/svg/hLLDRzim3BtxLmZkPK7NffSYQ144NLCN290DkHSx2BRY2dCaGvAp3Tl--_JbMfQrMZQMGo9HZuyC-KHn9017DMHGK0RkxnF-3HErFO6IqQRUaLV4i80aH-0soIl8EKYNAO0SB2yh-9Cs0gpNuCF-8xXuVCoocvi5s1lpPWD-9G0aVtmtn9YCfRmowPLbj_Ap09co7c290eureqTSeUN3IyCH-sfuF6DaEfS5572OKwq2Ay-3z7n-NgsKfvkDxmUPmBcC_iAhfhGPzMqpI561PHiCcLd7a1mfEmr8UedrJD5v3QahMc9YAFLoHdQsQ_qMZjDC5iFasliCHtYcKlc8XmEy7Sy8nL29kK2sqUrkHZ4v4WAJqgROxkOt-0iyQohvgzstqltkRhLwUbg0BJtLb20IkDW7XYeA2nbxHmgSmrR30ViaMYWb1WMcHdBTTfn4Lu3hXVfwPUZOyM9Ie1-QKgyYsEp5vXt79sFvZ0JKJCu-dayjbPewMWazIU2M4W4niGhnZH41SkF2Xn6oRiIOvRHqzUnswh2NqjMJssYmsqRG-bosXb8rPuo-ksMrflzGsHsf6-5MLHiYY8DiBb5yLnowj7srKd1zlJ4NS60CNIy5KXgt42Dfp-ZSdoGDFauMw8hvVUH0dqdQUwWip8eiIKClpm3ZF2Nsp55LaePdk__7H-Qp3-yFtM3O_ZWDGUmWSMlOYZS9N6MtWaQ7OoW9QEzh8C4tbQn_Nrs1jlU1Ws130xXU9_zzFaRZzZDmt7lqtmFq-nq1z7iUSF7w7a1uRG_fBpRVgOv-_dy1)

and of [Morstis/misp-frontend](https://github.com/Morstis/misp-frontend) (sorry for linking to this publicly, I needed some examples^^):
![Rendered preview](https://plantuml.feathecutie.de/plantuml/svg/nLVVRzem47xdhpXbBwiwIlSmeKOCRGrKciHKjBL_W0b7uc7io7PesTR_VNP-T2dGu6mFVlETl-_ouSDtPuKQ56P44YqaX65FhIctvWOIAVBi2qzHKaruZF2X5pEY50I308Y2mJaZEv5h68tWxUmTlBc-daulByzWLhh7O_ZL0-WzrJIKKiY2-abjaMdyeGPhgiEms2Z2H2RCrTLmE9cSGIGscU38ZqIg1MGamO8tDxoRc-7GVdvkdOSKt9fG9QzOEh3FbqIJi71DpAeHJA7Mb2UgZ5TRtGJb2ilJA5R_8oTASIi5d-6g_87bwVOtH9lS4TQdiU0g31kmquTzhkXFX8yGlCTDOEq7kjIfSLY_jLEaIQeRZbWmaqN3Y7CfJQJ8si61oebuu0ugBSoZollCGUDzvb7XDqjp45dWlEWa3FcIo5QD5SQ7lkyfHRQCv8d_OmFcG-NdE1OvRqrq1V6eBg8oPkYWiGJwK3gdhLassvvKtQvM9uGPX0zjqnsMwRhDDQEyDRtxM1zwlvA4qzMkJMO5yL8DHT9Q24NYfGPMaykKQrcUt0kFWoH65iX2K5eQreic5QyNuh68MHiEuM8BnMK82o4O4jwKoZ3MbKQx7Dq1FchJFbJrzR53_2rXEOPmP-86zG0RmCN568BJKrFGaLECEXqvPl3eo2W70MTItPLT2SqswSgfcrP7VDL6N5csf7V0saBh2Z-eVQx4kWWvfyEKXLwmzuAeACztw3L0TILUtsvUxjxp3VgNV-xeTxzltYnb-2CRZIVLiUqzVKSGVzz0mHy0)
