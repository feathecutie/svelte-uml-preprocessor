import type { PreprocessorGroup } from "svelte/compiler";
export default function umlPreprocess(file: string, syntax: "mermaid" | "plantuml"): PreprocessorGroup;
//# sourceMappingURL=index.d.ts.map