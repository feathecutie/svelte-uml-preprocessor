export declare function optional(strings: TemplateStringsArray, ...exps: any[]): string;
export declare function findModuleRoot(file: string): string;
/**
 * Check if the component source has a non module script tag.
 * @param {string} src
 */
export declare function hasScriptTag(src: string): boolean;
//# sourceMappingURL=utils.d.ts.map