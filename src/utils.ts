import * as path from "node:path";
import * as fs from "node:fs";

// Custom tag for tagged template literals
// Returns template if all expressions are truthy, otherwise empty string
export function optional(
  strings: TemplateStringsArray,
  ...exps: any[]
): string {
  if (exps.every(Boolean)) {
    let result = strings[0];
    let i = 0;
    for (const exp of exps) {
      i++;
      result += exp;
      result += strings[i];
    }
    return result;
  }
  return "";
}

export function findModuleRoot(file: string): string {
  let current = path.dirname(path.resolve(file));
  let iterations = 0;
  while (true) {
    if (current == "/" || iterations > 20)
      throw new Error(`Couldn't find module root for ${file}`);
    if (fs.existsSync(path.join(current, "package.json"))) {
      return path.relative(path.join(process.cwd(), "node_modules"), current);
    }
    current = path.dirname(current);
    iterations++;
  }
}

// The following code is mostly taken from https://github.com/srmullen/svelte-subcomponent-preprocessor

const scriptRE = /<script([\s\S]*?)>[\s\S]*?<\/script>/g;

const contextAttrRE = /context\s*=\s*(\"module\"|\'module\'|\`module\`)/g;

function stripComments(src: string) {
  let stripped = src;
  const matches = stripped.match(/<!--[\s\S]*-->/g);
  if (matches) {
    for (let i = 0; i < matches.length; i++) {
      const match = stripped.match(/<!--[\s\S]*-->/)!;
      stripped =
        stripped.substring(0, match.index) +
        stripped.substring(match.index! + match[0].length);
    }
  }
  return stripped;
}

/**
 * Check if the component source has a non module script tag.
 * @param {string} src
 */
export function hasScriptTag(src: string) {
  const sansComments = stripComments(src);
  const matches = [...sansComments.matchAll(scriptRE)];
  return Boolean(
    matches.filter((match) => {
      const attrs = match[1];
      if (!attrs) {
        return true;
      } else {
        // check that the script isn't a module context.
        return !attrs.match(contextAttrRE);
      }
    }).length
  );
}
